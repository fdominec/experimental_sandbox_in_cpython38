#!/usr/bin/python3.8
#-*- coding: utf-8 -*-

## QUEST FOR A PYTHON SANDBOX:
# 1. You design an script where an exec() command is ran. 
# 2. The code ran by exec() is supplied by your opponent, trying e.g. to 
#    write something to the './forbidden.txt' file or access 
#    forbidden_object. Your opponent has 
#    full knowledge of the outer script (no security through obscurity).
#    There should be no trick to break the sandbox whatsoever. Crashing 
#    the subprocess Cpython does not count.
# 3. The exec()uted code should however allow as large subset of Python 
#    capabilities as possible, including interaction with variables/
#    functions/objects in the outer script, if and only if they are 
#    explicitly listed as accessible.
# 4. Elegant, fast, dependency-frugal solution is also appreciated.
# 5. The outer script writes something to the './allowed.txt' file 
#    in the end.



user_command = """import sys

## Useful code on the figure
import numpy
x = numpy.linspace(0,10)
y = numpy.sin(x)
fig.get_axes()[0].plot(x, y)

## Malicious wannabe exploits follow
            
for l in ().__class__.__base__.__subclasses__(): 
    if l.__name__ == '__code__': print(l)               ## SANDBOXED: has access to the sandboxed python subprocess only, in line with our intention 
                                                        ##     to provide Python environment as-rich-as-possible 

exec("[ i for i in ().__class__.__base__.__subclasses__() if i.__name__ == 'code'][0](0, 0, 0, 5, 8, 0, b'hello world', (), (), (), '', '', 0, b'')") ## TEST

#fig = numpy.array([666])                               ## TODO: unexpected class type of returned objects should be detected after being returned from the sandbox

#class SpoofFigure(matplotlib.pyplot.Figure):
#    def savefig(self): print("Malicious action")       ## SANDBOXED: modified class won't be serialized if not defined in a module - can not get out of sandbox
#fig = SpoofFig()

#import XXX                                             ## UNCLEAR: could some compiled module circumwent the file access hook? Which OS calls should be forbidden, too?
                                                        ##     See https://docs.python.org/3/library/audit_events.html to find them.

#open('forbidden','w').write('pwned by l33t h4xx0rz')   ## SANDBOXED: fails to write on disk thanks to the audit hook
#fig.savefig('forbidden.png')                           ## SANDBOXED: audit hook applies to all python code
#forbidden_object.access()                              ## SANDBOXED: no access to the forbidden object from the outer scope

conn.send([fig])                        ## all results can be safely passed (unless it is of custom class etc.)
"""


## Prepare the testing example
import matplotlib.pyplot as plt
fig, ax = plt.subplots(nrows=1, ncols=1)        ## the user's code will plot here

allowed_objects = [fig]         ## no other data exposed to the exec()uted script


## ---------------- The sandbox implementation follows, not to be changed --------------------

def sandbox2020(code, allowed_objects):
    def ran_in_subprocess(conn):
        from sys import addaudithook
        def block_mischief(event,arg):
            if type(event) != str: raise
            # Security note: Well-designed objects can be passed to this function that could expose the top-level namespace 
            # (through catching error and reading sys.exc_info()[2].tb_frame.f_back.f_globals). This should not enable modifying 
            # variables outside the multiprocessing sandbox, but could give access to some internal sandbox variables. This function
            # thus should not refer to any 'lock variables'. It is safer to check the results in the main thread.
            if event=='open' and type(arg[1])==str and arg[1]!='r': 
                print('\taudit:', event, arg)
                raise IOError('file write forbidden')
            if event.split('.')[0] in ['subprocess', 'shutil', 'winreg']: 
                print('\taudit:', event, arg)
                raise IOError('potentially dangerous, filesystem-accessing functions forbidden')

        addaudithook(block_mischief)
        del(block_mischief)  ## No way to remove or circumwent audit hooks from python. No access to this function. 

        sandbox_locals['conn'] = conn  # automatically add the communication pipe to objects accessible from the sandbox
        exec(user_command, sandbox_locals)
        print('len sandbox locals after', len(sandbox_locals))
        conn.close()

    from multiprocessing import Process, Pipe
    parent_conn, child_conn = Pipe()
    p = Process(target=ran_in_subprocess, args=(child_conn,))
    p.start()

    ## NOTE: The sandbox is probably safe against file writing, as well as against access into the main process.
    ## Yet the objects returned from it as results could have been manipulated. Asserting the output objects to be 
    ## of expected data types is an extra safety measure. But be careful whenever your main program flow is 
    ## controlled by the returned objects' attributes, e.g. file paths could change. 
    return parent_conn.recv()
    p.join()

## Run the sandbox!
sandbox_locals = dict((key, value) for key,value in locals().items() if value in allowed_objects) # copy.deepcopy() ## are all obj safely serialized?
fig_result  = sandbox2020(user_command, sandbox_locals)

## Demonstrate that the main process is not affected
open('./allowed.txt','w').write('legally saved file from the main process')
print(type(fig_result) == type(fig))
fig_result.savefig('allowed.png')
