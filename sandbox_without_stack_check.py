#!/usr/bin/python3.8
#-*- coding: utf-8 -*-

#open('./allowed', 'w').write('a')

#import inspect


#exec("open('./forbidden', 'w').write('a')")
#
#for l in inspect.stack(): print('exec' in l, l)
#print()

#inspect=False    # wicked user can manipulate the 'inspect' module to obscure the 'exec' environment, or not?

#exec("for l in inspect.stack(): print('exec' in l, l)")




from sys import addaudithook
def block_mischief(event,arg):
    if 'WRITE_LOCK' in globals():
        if type(event) != str: raise
        # security note: well-designed objects can be passed to this function that could expose the top-level namespace 
        # (through catching error and reading sys.exc_info()[2].tb_frame.f_back.f_globals), which enables modifying 
        # variables outside the sandbox and removing
        # the WRITE LOCK; careful type checking is needed here!
        #if event=='open' and type(arg[1])==str and arg[1]!='r':  XXX
        if event=='open' and type(arg[1])==str and arg[1]!='r': #XXX 
            print('\taudit:', event, arg)
            raise IOError('file write forbidden')
        if event.split('.')[0] in ['subprocess', 'shutil', 'winreg']: 
            print('\taudit:', event, arg)
            raise IOError('potentially dangerous, filesystem-accessing functions forbidden')

addaudithook(block_mischief)
del(block_mischief)





#Tell me, Mr. Anderson, what good is an exc_info call when you are unable to raise an exception?
import multiprocessing
#import threading


print('x')
WRITE_LOCK = True
FANTOMAS = [True]
print('FANTOMAS', 'FANTOMAS' in locals())

#my_locals = copy.deepcopy(dict(locals()))
import copy


sandbox_locals = dict([copy.deepcopy(list(l)) for l in locals().items() if type(l[1]) is not type(copy)])
print('ISSAME', sandbox_locals['FANTOMAS'] is FANTOMAS)
exec("""
import sys
import numpy
print(locals())
def r(a, b):
    try:
        raise Exception()
    except:
        #del sys.exc_info()[2].tb_frame.f_back.f_globals['FANTOMAS'] 
        sys.exc_info()[2].tb_frame.f_back.f_globals['FANTOMAS'].append('23423')
        del sys.exc_info()[2].tb_frame.f_back.f_globals['WRITE_LOCK']
import matplotlib
import sys
w = type('evil',(str,),{'__ne__':r})()
sys.audit('open', None, w)
open('forbidden','w').write('pwned by l33t h4xx0rz')

output=numpy.eye(10)
""", sandbox_locals)
print(sandbox_locals)
print(locals)

print('FANTOMAS', 'FANTOMAS' in locals(), FANTOMAS )
